# -*- coding: UTF-8 -*-
# Copyright (C) 2017-2018 Schmidt Cristian Hernán

import os
import re
import sys
import bs4
import time
import random
import shutil
import inspect
import logging
import requests
import datetime
import functools
import collections

from pprint import pformat
from telegram import version as tg_version
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from telegram.error import Unauthorized, BadRequest, TimedOut

from gldt import set_gldt
from cache import timed_cache, TelegramCache
from tools import get_link, text_sorter, texdiff
from tools import remove_diacritics, spaces_normalized, escape_html, chunked
from tools import HTTPS_RE, NO_NAME_RE, EVIL_RE, YES_RE
from tools import SUB0_RE, SUB1_RE
from usages import usage, usage_admin, usage_examples
from database import DatabaseEngine, User, Group, Dist, Distname, find_distname
from database import WELCOME, GOODBYE, GOODBYE_KICKED, DISTNAME_MAX_DIFF

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)

BOT_TG_URL = os.environ["BOT_TG_URL"]
BOT_TOKEN = os.environ["BOT_TOKEN"]
BOT_BIND = os.environ["BIND"]
BOT_HOST = os.environ["HOST"]
BOT_PORT = int(os.environ['PORT'])
FID = int(os.environ["FID"])
BID = int(os.environ["BID"])

HELP = '/help@{}'.format(BOT_TG_URL.split("t.me")[1].strip("/"))

TASK = 7 * 24 * 3600  # time delta between ask, in seconds

TGC = TelegramCache()
DBE = DatabaseEngine()

USER_AGENTS = (
    'Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0',
    'Mozilla/5.0 (Windows NT 6.1; Win64; rv:60.0) Gecko/20100101 Firefox/60.0',
    'Googlebot/2.1 (+http://www.google.com/bot.html)',
)


def unknown_distribution(db, name, evil=True):
    if evil and EVIL_RE.search(name):
        return "Se detectó un nombre <b>malévolo</b> <i>¡debéis expiar tus pecados!</i>."
    else:
        message = "Distro desconocida: <code>{}</code>".format(escape_html(name))
        similar = []
        for distname in db.query(Distname).all():
            diff = texdiff(name, distname.name)
            if diff < DISTNAME_MAX_DIFF:
                similar.append((diff, "• {}".format(escape_html(distname.name))))
        if similar:
            similar.sort()
            message += "\nParecidos:\n{}".format('\n'.join(i[1] for i in similar))
        return message


def get_name(member):
    for name in (member.first_name, member.last_name):
        if name:
            name = NO_NAME_RE.sub('', name).strip().split()
            if name and name[0]:
                return name[0].capitalize()[:32]


def get_group_from_db(db, id, title):
    group = db.query(Group).filter_by(id=id).first()
    if group:
        if title != group.title:
            group.title = title
    else:
        group = Group(id=id, title=title)
        db.add(group)
        bot = db.query(User).filter_by(id=BID).first()
        bot.groups.append(group)
    return group


def get_context(db, member, chat=None, internal=False):
    user = None
    group = None
    if not member.is_bot or member.id == BID:  # excludes bots except myself
        user = db.query(User).filter_by(id=member.id).first()
        name = get_name(member)
        if user:
            new = False
            if name != user.name:
                user.name = name
            if member.username != user.username:
                user.username = member.username
        else:
            new = True
            user = User(id=member.id, name=name, username=member.username)
            db.add(user)

        if chat:
            if chat.type in [chat.GROUP, chat.SUPERGROUP]:
                group = get_group_from_db(db, chat.id, chat.title)
                if group not in user.groups:
                    user.groups.append(group)

            elif chat.type == chat.PRIVATE:
                if not user.can_talk:
                    user.can_talk = True

        if not internal:
            now = datetime.datetime.utcnow()
            delta = (now - user.last_interaction).total_seconds()
            logger.debug("delta={}".format(delta))
            if not new and delta < 0.5:  # prevents DoS
                logger.warning("DoS: delta={} user={}".format(delta, user.id))
                user = None
                group = None
            else:
                user.last_interaction = now

    if chat:
        return user, group
    else:
        return user


def get_context_from_reply(update, db):
    if update.message.reply_to_message:
        member = update.message.reply_to_message.from_user
        if member.id != BID:
            return get_context(db, member, internal=True)


def add_dists(db, user, name):
    distname = find_distname(db, name)
    if distname:
        user.distnames.append(distname)
        return True, distname.name_html
    else:
        return False, unknown_distribution(db, name)


def get_parents(dist):
    parents = []
    if dist.parent:
        parents.append(dist.parent)
        parents.extend(get_parents(dist.parent))
    return parents


def get_admins(update, db, group):
    if group:
        if update.message.chat.all_members_are_administrators:
            for user in db.query(User).filter(User.groups.contains(group)).all():
                yield user
        else:
            for member in TGC.get_admins(group.id):
                yield get_context(db, member, internal=True)


def is_admin(update, user, group):
    if update.message.chat.all_members_are_administrators:
        return True
    else:
        return TGC.is_admin(group.id, user.id)


# ---------------------------------------------------------------------------- #


def notify_user(bot, update, user):
    if user.can_ask and not user.distnames:
        now = datetime.datetime.utcnow()
        dt = (now - user.notified).total_seconds()
        if dt > TASK:
            user.notified = now
            text = "¡Hola {} tanto tiempo! ¿podrías establecer tu distro? 😊 con {} obtienes ayuda."
            bot.sendMessage(chat_id=update.message.chat_id,
                text=text.format(user.link_html, HELP), parse_mode="HTML")


def startup(func):
    # Not for channels
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        db = DBE.get_session()
        logger.debug("db open")
        try:
            bot = kwargs.get("bot", args[0])
            update = kwargs.get("update", args[1])
            user, group = get_context(db, update.message.from_user, update.message.chat)
            respond = bool(user)  # excludes bots except myself and prevents DoS
            if user and user.ignored:
                respond = False
            if group and group.ignored:
                respond = False
            if respond:
                kwargs["db"] = db
                kwargs["user"] = user
                kwargs["group"] = group
                ret = func(*args, **kwargs)
                if isinstance(ret, dict):
                    notify = ret.pop("notify_user", True)
                else:
                    notify = True
                if notify:
                    notify_user(bot, update, user)
        except:
            db.rollback()
            logger.debug("db rollback")
            raise
        else:
            db.commit()
            logger.debug("db commit")
        finally:
            db.close()
            logger.debug("db close")
    return wrapper


def get_arg(text):
    cmd, *arg = text.split(None, 1)
    if cmd.startswith("/") and len(cmd) > 1 and arg:
        return arg[0].strip()[:4096]
    else:
        return ""


def remove_needless(arg, needless, update):
    chat_id = update.message.chat_id
    bot = update.message.bot
    texts = []
    for o in needless(arg):
        arg = "{} {}".format(arg[:o.start()], arg[o.end()+1:])
        text = "La palabra <code>{}</code> es un ejemplo del {} no hay que incluirla."
        texts.append(text.format(o.group(), HELP))
    if texts:
        bot.sendMessage(chat_id=chat_id, text="\n".join(texts), parse_mode="HTML")
    return spaces_normalized(arg).strip()


def cmd_with_arg(*args, **kwargs):
    if len(args) > 0 and inspect.isfunction(args[0]):
        func = args[0]
        allows = lambda arg: True
        needless = None
        emptiness = False
    else:
        func = None
        allows = re.compile(kwargs.get("allows", ".")).match
        needless = kwargs.get("needless")
        needless = re.compile(needless).finditer if needless else None
        emptiness = kwargs.get("emptiness", False)

    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            update = kwargs.get("update", args[1])
            arg = get_arg(update.message.text)
            if needless:
                arg = remove_needless(arg, needless, update)
            if arg or emptiness:
                if allows(arg):
                    kwargs["arg"] = arg
                    return func(*args, **kwargs)
                else:
                    text = "Argumento incorrecto, vea {}."
                    update.message.reply_text(text.format(HELP), parse_mode="HTML")
            else:
                text = "Este comando necesita parámetros, vea {}."
                update.message.reply_text(text.format(HELP), parse_mode="HTML")
        return wrapper

    if func:
        return decorator(func)
    else:
        return decorator


def cmd_without_arg(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        update = kwargs.get("update", args[1])
        arg = get_arg(update.message.text)
        if arg:
            text = "Este comando no necesita parámetros, vea {}."
            update.message.reply_text(text.format(HELP), parse_mode="HTML")
        else:
            return func(*args, **kwargs)
    return wrapper


def admins(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        bot = kwargs.get("bot", args[0])
        update = kwargs.get("update", args[1])
        db = kwargs.get("db")
        user = kwargs.get("user")
        group = kwargs.get("group")
        if is_admin(update, user, group):
            return func(*args, **kwargs)
        else:
            text = "ℹ esta es una función administrativa."
            update.message.reply_text(text, parse_mode="HTML")
    return wrapper


def father(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        update = kwargs.get("update", args[1])
        if update.message.from_user.id == FID:
            kwargs["arg"] = get_arg(update.message.text)
            return func(*args, **kwargs)
    return wrapper


# ---------------------------------------------------------------------------- #


def get_greetings(user, group, template):
    context = {
        'name': user.link_html,
        'link': user.link_html,
        'group': group.title_html,
    }
    return template.format(**context)


def show_greetings(bot, user, group, template, generic, error, suffix=""):
    config = {
        'parse_mode': 'HTML',
        'disable_web_page_preview': True,
    }
    try:
        text = get_greetings(user, group, template) + suffix
        if len(text) > 4096:
            raise ValueError("Mensaje muy largo modifique la plantilla.")
        bot.sendMessage(chat_id=group.id, text=text, **config)
    except Exception as e:
        for text in ('{} ({})'.format(error, e), get_greetings(user, group, generic)):
            bot.sendMessage(chat_id=group.id, text=text, **config)


def get_list_admins(bot, update, db, group, preffix="\n"):
    text = [preffix]
    if group.show_admin_in_welcome:
        text.append("Lista de administradores:")
        lst = [u.user_html for u in get_admins(update, db, group)]
        if group.show_admin_in_random_order:
            seed = time.time() + int.from_bytes(os.urandom(4), byteorder="big")
            random.seed(seed)
            random.shuffle(lst)
        else:
            lst.sort(key=text_sorter)
        for u in lst:
            text.append('• {}'.format(u))
    return '\n'.join(text)


def delete_empty_groups(db):
    counter = 0
    for group in db.query(Group).all():
        if len(group.users) < 2 or not any(u.id == BID for u in group.users):
            counter += 1
            db.delete(group)
            logger.debug("DELETE {}".format(group))
    return counter


@startup
def new_user_cmd(bot, update, db, user, group):
    for new_user in update.message.new_chat_members:
        user = get_context(db, new_user, internal=True)
        if group.show_greetings and group.welcome and user and user.id != BID:
            adms = get_list_admins(bot, update, db, group)
            error = "⚠️ Error en la plantilla de la bienvenida."
            show_greetings(bot, user, group, group.welcome, WELCOME, error, adms)
    return {"notify_user": False}


@startup
def bye_user_cmd(bot, update, db, user, group):
    u = get_context(db, update.message.left_chat_member, internal=True)
    if u:
        if group:
            if group in u.groups:
                u.groups.remove(group)
            if u.id == BID:
                for uu in db.query(User).all():
                    if group in uu.groups:
                        uu.groups.remove(group)
                db.delete(group)
        else:
            u.can_ask = False
        delete_empty_groups(db)
        kicked = u.id != user.id
        a = group.goodbye and not kicked
        b = group.goodbye_kicked and kicked
        if group.show_greetings and (a or b) and u.id != BID:
            error = "⚠️ Error en la plantilla de la despedida."
            if kicked:
                template = group.goodbye_kicked
                generic = GOODBYE_KICKED
            else:
                template = group.goodbye
                generic = GOODBYE
            show_greetings(bot, u, group, template, generic, error)
    return {"notify_user": False}


@startup
def change_title_cmd(bot, update, db, user, group):
    pass  # already updated in the startup decorator


@startup
def migrate_cmd(bot, update, db, user, group):
    id_old = group.id
    id_new = update.message.migrate_from_chat_id or update.message.migrate_to_chat_id

    group_new = get_group_from_db(db, id_new, group.title)

    for u in db.query(User).all():
        if group in u.groups:
            u.groups.remove(group)
            u.groups.append(group_new)

    delete_empty_groups(db)

    message = "Completed migration: {}, {} to {}"
    logger.debug(message.format(group.title, id_old, id_new))


@startup
def conversation_cmd(bot, update, db, user, group):
    chat = update.message.chat
    text = update.message.text
    if chat.type == chat.PRIVATE and text:
        if text.startswith("/"):
            text = "Comando desconocido, vea {}.".format(HELP)
        else:
            text = "Solo respondo a mis comandos, vea {}.".format(HELP)
        update.message.reply_text(text, parse_mode="HTML")


@startup
def start_cmd(bot, update, db, user, group):
    text = "¡Hola {}! con el comando {} obtienes ayuda.".format(user.name_html, HELP)
    update.message.reply_text(text, parse_mode="HTML")


@startup
def help_cmd(bot, update, db, user, group):
    adm = usage_admin if not group or is_admin(update, user, group) else ""
    help = usage.format(adm)
    bot.sendMessage(chat_id=update.message.chat_id, text=help,
        parse_mode="HTML", disable_web_page_preview=True)


def only_in_groups(update, user):
    text = "Este comando se usa en grupos"
    groups = sorted(g.title_html for g in user.groups)
    if groups:
        if len(groups) > 1:
            g = "{} y/o {}".format(", ".join(groups[:-1]), groups[-1])
        else:
            g = groups[0]
        text += ", pruébame en {}.".format(g)
    else:
        text += ", debes estar en uno que yo también esté."
    update.message.reply_text(text, parse_mode="HTML")


@startup
def only_in_groups_cmd(bot, update, db, user, group):
    only_in_groups(update, user)


# ---------------------------------------------------------------------------- #


@startup
@cmd_with_arg(allows="\w", needless="(distro|nombre|texto)")
def add_cmd(bot, update, db, user, group, arg):
    ok, text = add_dists(db, user, arg)
    if ok:
        text = "<code>{}</code> fue agregada a tu lista.".format(text)
    update.message.reply_text(text, parse_mode="HTML")


@startup
@cmd_with_arg(allows="(^\*$|\w)", needless="(distro|nombre|texto)")
def rem_cmd(bot, update, db, user, group, arg):
    if arg == "*":
        n = len(user.distnames)
        if n > 1:
            text = "Todas tus distros fueron eliminadas"
        elif n == 1:
            text = "Tu distro fue eliminada"
        else:
            text = "No tienes distros en tu lista"
        text += (", incluida la favorita." if user.favourite else ".")
        user.distnames.clear()
        user.favourite = None
    else:
        distname = find_distname(db, arg, user)
        if distname and distname in user.distnames:
            user.distnames.remove(distname)
            name = distname.name_html
            text = "<code>{}</code> fue eliminada de tu lista.".format(name)
        else:
            text = "<code>{}</code> no está en tu lista.".format(arg)
    update.message.reply_text(text, parse_mode="HTML")


@startup
@cmd_with_arg(allows="(^-$|\w)", needless="(distro|nombre|texto)")
def fav_cmd(bot, update, db, user, group, arg):
    if arg == "-":
        if user.favourite:
            user.favourite = None
            text = "Se eliminó tu distro favorita."
        else:
            text = "No tienes una distro favorita."
    else:
        distname = find_distname(db, arg)
        if distname:
            user.favourite_id = distname.id
            user.distnames.append(distname)
            name = distname.name_html
            text = "<code>{}</code> es tu distro favorita ahora.".format(name)
        else:
            text = unknown_distribution(db, arg)
    update.message.reply_text(text, parse_mode="HTML")


def get_yes_no(db, obj, attr, answer):
    status = getattr(obj, attr)
    if YES_RE.match(remove_diacritics(answer)):
        if not status:
            setattr(obj, attr, True)
        return True
    else:
        if status:
            setattr(obj, attr, False)
        return False


@startup
@cmd_with_arg(allows="^\w+$")
def ask_cmd(bot, update, db, user, group, arg):
    yes = get_yes_no(db, user, "can_ask", arg)
    if yes:
        text = "Genial, solo te haré acordar si no tienes distros en tu lista."
    else:
        text = "Entendido, no te molestaré con las distros."
    update.message.reply_text(text, parse_mode="HTML")
    return {"notify_user": False}


# ---------------------------------------------------------------------------- #


@startup
@cmd_with_arg(allows="(\w+|)", needless="(distro|nombre|texto)", emptiness=True)
def show_cmd(bot, update, db, user, group, arg):
    ur = get_context_from_reply(update, db)
    if ur:
        u = ur
    elif arg:
        if group:
            if arg.isdigit():
                query = db.query(User).filter_by(id=int(arg))
                query = query.filter(User.groups.contains(group))
                u = query.first()
            else:
                for n in arg.split():
                    for p, r in ((SUB0_RE, "_"), (SUB1_RE, "%")):
                        n = p.sub(r, n)
                        for var in (User.username, User.name):
                            query = db.query(User).filter(var.ilike(n))
                            query = query.filter(User.groups.contains(group))
                            u = query.first()
                            if u:
                                diff = texdiff(arg, getattr(u, var.expression.name))
                                if diff < DISTNAME_MAX_DIFF:
                                    break
                                else:
                                    u = None
        else:
            only_in_groups(update, user)
            return
    else:
        u = user
    if u:
        adm = " ⚔️ " if group and is_admin(update, u, group) else ""
        name = "<code>{}</code>{}".format(u.nick_html, adm)
        distnames = []
        for distname in u.distnames:
            if u.favourite:
                fav = " ❤️" if distname == u.favourite else ""
            else:
                fav = ""
            distnames.append("{}{}".format(distname.name_html, fav))
        if distnames:
            text = "{}: {}".format(name, ' | '.join(distnames))
        else:
            text = "{} no tiene distros establecidas.".format(name)
    else:
        text = "El usuario <code>{}</code> no está en la base de datos.".format(escape_html(arg))
    bot.sendMessage(chat_id=update.message.chat_id, text=text, parse_mode="HTML")


@startup
@cmd_with_arg(allows="(\w+|)", needless="(distro|nombre|texto)", emptiness=True)
def dist_cmd(bot, update, db, user, group, arg):
    if arg:
        distname = find_distname(db, arg)
        if distname:
            lines = ["<b>{}</b>".format(distname.dist.names[-1].name_html)]
            if len(distname.dist.names) > 1:
                lines.append("Nombres:")
                for dn in distname.dist.names:
                    url = get_link(dn.url, maxlen=40-len(dn.name))
                    lines.append(" ├─ {}: {}{}".format(dn.start, dn.name_html, url))
                lines.append(lines.pop().replace("├", "└"))
            else:
                lines.append("Comienzo: {}".format(distname.start))
                lines.append("Información: {}".format(get_link(distname.url)))

            if distname.dist.stop_year:
                lines.append("Finalizó: {}".format(distname.dist.stop))

            parents = get_parents(distname.dist)
            if parents:
                names = ' → '.join(dist.names[-1].name_html for dist in parents)
                lines.append("Deriva de: {}".format(names))

            text = "\n".join(lines)
        else:
            text = unknown_distribution(db, arg)
    elif group:
        global_fav = collections.defaultdict(int)
        group_fav = collections.defaultdict(int)

        for u in db.query(User).all():
            if u.favourite:
                global_fav[u.favourite.name_html] += 1
                if group in u.groups:
                    group_fav[u.favourite.name_html] += 1

        def remove_solitaires(dic):
            lst = []
            for key, val in dic.items():
                if val == 1:
                    lst.append(key)
            for key in lst:
                dic.pop(key)
            return lst

        global_fav_sol = remove_solitaires(global_fav)
        group_fav_sol = remove_solitaires(group_fav)

        set1 = set(list(global_fav) + list(group_fav))
        set2 = set(global_fav_sol + group_fav_sol)
        set2 = set2.difference(set1)

        if not set1:
            set1 = set2
            set2 = set()

        if set1:
            lines = ["N° de distros favoritas:  grupal | global"]
            lines.append('')
            line = "• <b>{}</b>:  {:.0f} | {:.0f}".format
            for dn in sorted(set1, key=text_sorter):
                lines.append(line(dn, group_fav[dn], global_fav[dn]))
            if set2:
                lines.append('')
                lines.append('Otras (N°≤1): ' + ', '.join(sorted(set2, key=text_sorter)))
        else:
            lines = ["Los usuarios de este grupo aún no establecieron ninguna distro"]

        text = "\n".join(lines)
    else:
        only_in_groups(update, user)
        return
    bot.sendMessage(chat_id=update.message.chat_id, text=text, parse_mode="HTML",
        disable_web_page_preview=True)


@startup
@cmd_with_arg(allows="\w", needless="(distro|nombre|texto)")
def who_cmd(bot, update, db, user, group, arg):
    distname = find_distname(db, arg)
    if distname:
        lines = ["<b>{}</b>".format(distname.dist.names[-1].name_html)]
        sublines = []
        for dn in distname.dist.names:
            for u in dn.users:
                if group in u.groups:
                    sublines.append(" ├─ <i>{}</i>".format(u.user_html))
        if sublines:
            sublines.sort(key=text_sorter)
            sublines.append(sublines.pop().replace("├", "└"))
            lines.extend(sublines)
        else:
            lines.append(" └─ <i>nadie</i>")
        text = "\n".join(lines)
    else:
        text = unknown_distribution(db, arg)
    bot.sendMessage(chat_id=group.id, text=text, parse_mode="HTML")


@startup
@cmd_with_arg(allows="\w", needless="(distro|nombre|texto)")
def find_cmd(bot, update, db, user, group, arg):
    distnames = find_distname(db, arg, all=True)
    if distnames:
        lines = ["Distros encontradas:"]
        for distname in distnames:
            lines.append("• {}".format(distname.name_html))
        text = "\n".join(lines)
    else:
        text = unknown_distribution(db, arg, evil=False)
    update.message.reply_text(text, parse_mode="HTML",
        disable_web_page_preview=True)


@timed_cache(maxsize=128, minutes=6*60)
def get_html(url):
    for user_agent in USER_AGENTS:
        session = requests.Session()
        response = session.get(url, headers={'User-Agent': user_agent})
        if response.ok:
            logger.debug('Use User-Agent: %s', user_agent)
            break
        time.sleep(1 + random.random())  # wait 1 to 2 seconds
    else:
        raise ValueError("Connection problems")
    response.raise_for_status()
    response.encoding = response.apparent_encoding
    return bs4.BeautifulSoup(response.text, 'lxml')


@startup
@cmd_with_arg(allows="\w")
def alternative_cmd(bot, update, db, user, group, arg):
    host = 'http://alternativeto.net'
    head = 'Alternativa a: <b>{}</b>\n'
    line = '{}. <a href="{}">{}</a>{}\nDisponible en: {}\n'
    nalt = 'No se encontró alternativa para: <b>{}</b>'
    merr = ('Error en el scraping de la web de alternativeto.net '
            '(¡otra vez cambiando las estructuras! 😡), '
            'lo resolveré lo mas rápido que pueda.')
    try:
        # Search
        query = spaces_normalized(SUB0_RE.sub(" ", arg)).replace(" ", "+")
        url = "{}/browse/search/?q={}&ignoreExactMatch=true".format(host, query)
        logger.debug("alternative_cmd.url: {}".format(url))
        html = get_html(url)
        # App
        link = html.find(name="a", attrs={"data-link-action": "Search"})
        if link:
            path = link.attrs["href"]
            url = "{}{}".format(host, path)
            html = get_html(url)
            # Data
            name = link.get_text().strip()
            text = [head.format(name)]

            attrs = {"class": "container", "data-testid": "alternative-list"}
            alts = html.find(name="div", attrs=attrs).find(name="ul").children
            count = 1
            for alt in alts:
                n = alt.find(name="div", attrs={"data-testid": "app-header"})
                cp = alt.find(name="ul", attrs={"class": "badges"})

                if n and cp:
                    alt_url = "{}{}".format(host, n.find("a").attrs["href"])
                    alt_name = n.get_text().strip()

                    if cp:
                        cc = cp.find_all(name="li", attrs={"class": "badge-license"})
                        cc = [li.span.decode_contents().strip() for li in cc]
                        pricing = " ({})".format(", ".join(cc)) if cc else ""
                        platforms = cp.find_all(lambda tag: "li" == tag.name and
                                                            'class' not in tag.attrs)
                        platforms = ", ".join(li.get_text() for li in platforms)
                    else:
                        pricing = ""
                        platforms = "???"

                    text.append(line.format(count, alt_url, alt_name, pricing, platforms))
                    count += 1
            if count == 1:
                text = [nalt.format(escape_html(name))]
            else:
                # More info?
                nav = html.find(name="nav", attrs={"aria-label": "Pagination Navigation"})
                if nav and nav.find(name="a", text="Next"):
                    text.append("Más alternativas en {}".format(url))
        else:
            text = [nalt.format(escape_html(arg))]
    except Exception:
        logger.exception('scraping alternativeto.net')
        text = [merr]
    bot.sendMessage(chat_id=update.message.chat_id, text="\n".join(text),
        parse_mode="HTML", disable_web_page_preview=True)


@startup
def example_cmd(bot, update, db, user, group):
    bot.sendMessage(chat_id=update.message.chat_id, text=usage_examples,
        parse_mode="HTML")


# ---------------------------------------------------------------------------- #


@startup
@admins
@cmd_with_arg(allows="^\w+$")
def show_greetings_cmd(bot, update, db, user, group, arg):
    yes = get_yes_no(db, group, "show_greetings", arg)
    if yes:
        text = "Los saludos serán mostrados."
    else:
        text = "Los saludos no serán mostrados."
    update.message.reply_text(text, parse_mode="HTML")


@startup
@admins
@cmd_with_arg(allows="^\w+$")
def show_admin_in_welcome_cmd(bot, update, db, user, group, arg):
    yes = get_yes_no(db, group, "show_admin_in_welcome", arg)
    if yes:
        text = "Se mostrará la lista de administradores en la bienvenida."
    else:
        text = "No se mostrará la lista de administradores en la bienvenida."
    update.message.reply_text(text, parse_mode="HTML")


@startup
@admins
@cmd_with_arg(allows="^\w+$")
def show_admin_in_random_order_cmd(bot, update, db, user, group, arg):
    yes = get_yes_no(db, group, "show_admin_in_random_order", arg)
    if yes:
        text = "Se mostrará la lista de administradores en orden aleatorio."
    else:
        text = "Se mostrará la lista de administradores en orden alfabético."
    update.message.reply_text(text, parse_mode="HTML")


@startup
@admins
@cmd_with_arg(allows="(\w+|)", emptiness=True)
def set_welcome_cmd(bot, update, db, user, group, arg):
    try:
        if arg:
            get_greetings(user, group, arg)
            text = "La plantilla de la bienvenida fue establecida."
        else:
            arg = None
            text = "La plantilla de la bienvenida fue eliminada."
        group.welcome = arg
    except Exception as e:
        text = "⚠️ Error en la plantilla de la bienvenida ({}).".format(e)
    update.message.reply_text(text, parse_mode="HTML")


@startup
@admins
@cmd_with_arg(allows="(\w+|)", emptiness=True)
def set_goodbye_cmd(bot, update, db, user, group, arg):
    try:
        if arg:
            get_greetings(user, group, arg)
            text = "La plantilla de la despedida fue establecida."
        else:
            arg = None
            text = "La plantilla de la despedida fue eliminada."
        group.goodbye = arg
    except Exception as e:
        text = "⚠️ Error en la plantilla de la despedida ({}).".format(e)
    update.message.reply_text(text, parse_mode="HTML")


@startup
@admins
@cmd_with_arg(allows="(\w+|)", emptiness=True)
def set_goodbye_kicked_cmd(bot, update, db, user, group, arg):
    try:
        if arg:
            get_greetings(user, group, arg)
            text = "La plantilla de la despedida (por expulsión) fue establecida."
        else:
            arg = None
            text = "La plantilla de la despedida (por expulsión) fue eliminada."
        group.goodbye_kicked = arg
    except Exception as e:
        text = "⚠️ Error en la plantilla de la despedida ({}).".format(e)
    update.message.reply_text(text, parse_mode="HTML")


@startup
@admins
@cmd_without_arg
def test_greetings_cmd(bot, update, db, user, group):
    empty = "(Sin establecer)"
    adms = get_list_admins(bot, update, db, group)
    show_greetings(bot, user, group, group.welcome or empty, "", "", adms)
    show_greetings(bot, user, group, group.goodbye or empty, "", "")
    show_greetings(bot, user, group, group.goodbye_kicked or empty, "", "")


@startup
@admins
@cmd_without_arg
def reset_greetings_cmd(bot, update, db, user, group):
    group.welcome = WELCOME
    group.goodbye = GOODBYE
    group.goodbye_kicked = GOODBYE_KICKED
    text = "Plantillas restablecidas."
    update.message.reply_text(text, parse_mode="HTML")


# ---------------------------------------------------------------------------- #


@startup
@father
def maintenance_cmd(bot, update, db, user, group, arg):
    timedouts = 0
    del_users = 0
    del_group = 0
    del_relationships = 0

    for group in db.query(Group).all():
        try:
            for user in group.users:
                time.sleep(0.05)
                param = {
                    'chat_id': group.id,
                    'user_id': user.id,
                    'timeout': 10,
                }
                chatmember = bot.get_chat_member(**param)
                if chatmember.status == chatmember.LEFT:
                    user.groups.remove(group)
                    del_relationships += 1
                    logger.debug("DELETE {} FROM {}".format(group, user))

        except Unauthorized:
            db.delete(group)
            del_group += 1
            logger.debug("DELETE {}".format(group))

        except BadRequest:
            db.delete(user)
            del_users += 1
            logger.debug("DELETE {}".format(user))

        except TimedOut:
            timedouts += 1

    del_group += delete_empty_groups(db)
    info = "Eliminaciones:\n• Grupos: {}\n• Usuarios: {}\n• Relaciones: {}\n• Timedouts: {}"
    message = info.format(del_group, del_users, del_relationships, timedouts)
    update.message.reply_text(message, parse_mode="HTML")


@startup
@father
def gldt_cmd(bot, update, db, user, group, arg):
    key = datetime.datetime.utcnow().strftime("%Y-%m-%d %H-%M")
    if arg == key:
        errors = set_gldt(db, ('N', '#N', '//N'))
        if errors:
            err = '\nErrores:\n' + '\n'.join('• {}'.format(e) for e in errors)
        else:
            err = ''
        message = 'Listo.{}'.format(err)
    else:
        message = "<code>/gldt AAAA-MM-DD HH-MM (UTC)</code>"
    update.message.reply_text(message, parse_mode="HTML")


@startup
@father
def news_cmd(bot, update, db, user, group, arg):
    if arg:
        for g in db.query(Group).all():
            try:
                bot.sendMessage(chat_id=g.id, text=arg, parse_mode="HTML")
            except Exception as e:
                text = "[news] {}: {}".format(g.title_html, escape_html(str(e)))
                logger.debug(text)
                update.message.reply_text(text, parse_mode="HTML")
    else:
        text = "<code>/news</code> mensaje"
        update.message.reply_text(text, parse_mode="HTML")


def get_entity(arg, query, var):
    if arg[0] in ("'", '"'):
        start = 1
        stop = arg.find(arg[0], start)
    else:
        start = 0
        stop = arg.find(" ")
    if stop < start:
        start = 0  # not close the quotes
        stop = None
        text = None
    else:
        text = arg[stop+1:].strip()
    id_or_name = arg[start:stop].strip()
    if id_or_name.lstrip("-").strip().isdigit():
        entity = query.filter_by(id=id_or_name).first()
    else:
        name = id_or_name.replace("@", "")
        similar = []
        for q in query.all():
            value = getattr(q, var)
            if value:
                diff = texdiff(name, value)
                if diff < DISTNAME_MAX_DIFF:
                    similar.append((diff, q))
        if similar:
            similar.sort()
            entity = similar[0][1]
        else:
            entity = None
    return entity, text


@startup
@father
def say_cmd(bot, update, db, user, group, arg):
    if arg:
        g, text = get_entity(arg, db.query(Group), 'title')
        if g and text:
            bot.sendMessage(chat_id=g.id, text=text, parse_mode="HTML")
            return
    lines = ["<code>/say</code> id/name mensaje"]
    lines.append("Grupos:")
    for g in db.query(Group).all():
        lines.append("<code>{}</code> ─ {}".format(g.id, g.title_html))
    update.message.reply_text("\n".join(lines), parse_mode="HTML")


@startup
@father
def ignore_cmd(bot, update, db, user, group, arg):
    if arg:
        for entity, var in ((User, 'username'), (Group, 'title')):
            e, text = get_entity(arg, db.query(entity), var)
            if e:
                if text and e.id not in (FID, BID):
                    e.ignored = bool(YES_RE.match(remove_diacritics(text)))
                text = "Ignorar {}: {}".format(e.id, e.ignored)
                update.message.reply_text(text, parse_mode="HTML")
                return
    text = "<code>/ignore</code> id/name si/no"
    update.message.reply_text(text, parse_mode="HTML")


def error_cmd(bot, update, error):
    reason = "Error: {}".format(error)
    for line in '{}\n{}'.format(reason, pformat(update)).splitlines():
        logger.error(line)
    bot.send_message(FID, reason)


# ---------------------------------------------------------------------------- #


def run(polling=False, clean=False):
    logger.debug("Python version {}".format(sys.version.replace("\n", "")))
    logger.debug("Telegram version {}".format(tg_version.__version__))

    updater = Updater(BOT_TOKEN)
    TGC.bot = updater.bot

    db = DBE.get_session(create_all_tables=True)
    get_context(db, updater.bot.get_me(), internal=True)  # ensure bot exists in db
    db.commit()
    db.close()
    del db

    p = Filters.private
    g = Filters.group
    pg = g | p
    command_handlers = (
        ('start',                       p, start_cmd),
        ('help',                       pg, help_cmd),
        #
        ('add',                        pg, add_cmd),
        ('rem',                        pg, rem_cmd),
        ('fav',                        pg, fav_cmd),
        ('ask',                        pg, ask_cmd),
        #
        ('show',                       pg, show_cmd),
        ('dist',                       pg, dist_cmd),
        ('who',                         g, who_cmd,                         ~g, only_in_groups_cmd),
        ('find',                       pg, find_cmd),
        ('alternative',                pg, alternative_cmd),
        #
        ('example',                    pg, example_cmd),
        #
        ('show_greetings',              g, show_greetings_cmd,              ~g, only_in_groups_cmd),
        ('show_admin_in_welcome',       g, show_admin_in_welcome_cmd,       ~g, only_in_groups_cmd),
        ('show_admin_in_random_order',  g, show_admin_in_random_order_cmd,  ~g, only_in_groups_cmd),
        ('set_welcome',                 g, set_welcome_cmd,                 ~g, only_in_groups_cmd),
        ('set_goodbye',                 g, set_goodbye_cmd,                 ~g, only_in_groups_cmd),
        ('set_goodbye_kicked',          g, set_goodbye_kicked_cmd,          ~g, only_in_groups_cmd),
        ('test_greetings',              g, test_greetings_cmd,              ~g, only_in_groups_cmd),
        ('reset_greetings',             g, reset_greetings_cmd,             ~g, only_in_groups_cmd),
        #
        ('maintenance',                 p, maintenance_cmd),
        ('gldt',                        p, gldt_cmd),
        ('news',                        p, news_cmd),
        ('say',                         p, say_cmd),
        ('ignore',                      p, ignore_cmd),
    )
    for cmd, *sets in command_handlers:
        for filters, func in chunked(sets, 2):
            updater.dispatcher.add_handler(CommandHandler(cmd, func, filters))

    message_handlers = (
        (Filters.status_update.new_chat_members, new_user_cmd),
        (Filters.status_update.left_chat_member, bye_user_cmd),
        (Filters.status_update.new_chat_title, change_title_cmd),
        (Filters.status_update.migrate, migrate_cmd),
        (pg, conversation_cmd),
    )
    for filters, func in message_handlers:
        updater.dispatcher.add_handler(MessageHandler(filters, func))

    updater.dispatcher.add_error_handler(error_cmd)

    if polling:
        updater.bot.set_webhook()  # Delete webhook
        updater.start_polling(clean=clean)
    else:
        updater.bot.set_webhook('{}/{}'.format(BOT_HOST, BOT_TOKEN))
        updater.start_webhook(listen=BOT_BIND, port=BOT_PORT, url_path=BOT_TOKEN)

    updater.idle()


if __name__ == "__main__":
    run()
