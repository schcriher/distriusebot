# -*- coding: UTF-8 -*-
# Copyright (C) 2017-2018 Schmidt Cristian Hernán

import os
import re
import datetime

from sqlalchemy import create_engine, Column, Table, ForeignKey
from sqlalchemy import BigInteger, Integer, String, Boolean, DateTime
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.ext.declarative import declarative_base, declared_attr

from tools import (SUB3_RE, HEALTH_RULES, escape_html, texdiff,
                   spaces_normalized, percentage_normalized)

DISTNAME_MAX_DIFF = 0.36  # maximum difference in the name for the search

AT = "@\N{ZERO WIDTH NO-BREAK SPACE}"
WELCOME = "¡Te damos la bienvenida, {name}!"
GOODBYE = "Adiós {name} ¡que te vaya bien!"
GOODBYE_KICKED = "Adiós {name}."


class Base:
    @declared_attr
    def __tablename__(cls):
        # TableName -> table_name
        return re.sub(r'([a-z])([A-Z])', r'\1_\2', cls.__name__).lower()
Base = declarative_base(cls=Base)


user_group = Table('user_group', Base.metadata,
    Column('user_id', BigInteger, ForeignKey('user.id')),
    Column('group_id', BigInteger, ForeignKey('group.id')),
)


user_distname = Table('user_distname', Base.metadata,
    Column('user_id', BigInteger, ForeignKey('user.id')),
    Column('distname_id', Integer, ForeignKey('distname.id')),
)


class User(Base):
    id = Column(BigInteger, primary_key=True)
    name = Column(String(32))
    username = Column(String(32))
    favourite_id = Column(Integer, ForeignKey('distname.id'))
    favourite = relationship("Distname", foreign_keys=favourite_id)
    groups = relationship("Group", secondary=user_group, back_populates="users")
    distnames = relationship("Distname", secondary=user_distname, back_populates="users")
    notified = Column(DateTime(), default=datetime.datetime.utcnow)
    last_interaction = Column(DateTime(), default=datetime.datetime.utcnow)
    can_ask = Column(Boolean, default=True)
    can_talk = Column(Boolean, default=False)
    ignored = Column(Boolean, default=False)

    @property
    def name_html(self):
        return escape_html(self.name or self.username or str(self.id))

    @property
    def link_html(self):
        return '<a href="tg://user?id={}">{}</a>'.format(self.id, self.name_html)

    @property
    def nick_html(self):
        if self.username:
            string = "{}{}".format(AT, self.username)
        else:
            string = "{}".format(self.name or str(self.id))
        return escape_html(string)

    @property
    def user_html(self):
        if self.name and self.username:
            string = "{} ({}{})".format(self.name, AT, self.username)
        elif self.name:
            string = "{}".format(self.name)
        elif self.username:
            string = "{}{}".format(AT, self.username)
        else:
            string = "{}".format(self.id)
        return escape_html(string)

    def __repr__(self):
        return "User:{}:@{}".format(self.name, self.username)


class Group(Base):
    id = Column(BigInteger, primary_key=True)
    title = Column(String(64))
    users = relationship("User", secondary=user_group, back_populates="groups")
    ignored = Column(Boolean, default=False)
    show_greetings = Column(Boolean, default=True)
    show_admin_in_welcome = Column(Boolean, default=False)
    show_admin_in_random_order = Column(Boolean, default=True)
    welcome = Column(String(4096), default=WELCOME)
    goodbye = Column(String(4096), default=GOODBYE)
    goodbye_kicked = Column(String(4096), default=GOODBYE_KICKED)

    @property
    def title_html(self):
        return escape_html(self.title)

    def __repr__(self):
        return "Group:{}".format(self.title)


class Dist(Base):
    # https://github.com/FabioLolix/LinuxTimeline
    id = Column(Integer, primary_key=True)
    names = relationship("Distname", back_populates="dist")
    stop_year = Column(Integer)
    stop_month = Column(Integer)
    stop_day = Column(Integer)
    parent_id = Column(Integer, ForeignKey('dist.id'))
    parent = relationship("Dist", remote_side=[id])

    @property
    def stop(self):
        nums = []
        for num in (self.stop_year, self.stop_month, self.stop_day):
            if num:
                nums.append(str(num))
            else:
                break
        return "-".join(nums)

    def __repr__(self):
        return "Dist:{}:{}".format(self.id, len(self.names))


class Distname(Base):
    id = Column(Integer, primary_key=True)
    name = Column(String(64), nullable=False)
    start_year = Column(Integer)
    start_month = Column(Integer)
    start_day = Column(Integer)
    url = Column(String(256))
    dist_id = Column(Integer, ForeignKey('dist.id'))
    dist = relationship("Dist", back_populates="names")
    users = relationship("User", secondary=user_distname, back_populates="distnames")

    @property
    def name_html(self):
        return escape_html(self.name)

    @property
    def start(self):
        nums = []
        for num in (self.start_year, self.start_month, self.start_day):
            if num:
                nums.append(str(num))
            else:
                break
        return "-".join(nums)

    def __repr__(self):
        return "Distname:{}".format(self.name)


def get_min_name(name):
    # Cases such as OS4, OS2005 and 0Linux give a minimum name equal to ''
    return spaces_normalized(SUB3_RE.sub(" ", name)) or name


def find_distname(db, name, user=None, all=False, diffmax=DISTNAME_MAX_DIFF):
    min_name = get_min_name(name)
    n = spaces_normalized(name)
    if len(n) > 1:
        for rule in HEALTH_RULES:
            n = percentage_normalized(rule(n))
            query = db.query(Distname)
            if user:
                query = query.filter(Distname.users.contains(user))
            distnames = query.filter(Distname.name.ilike(n)).all()
            if distnames:
                if all:
                    return distnames
                else:
                    sort = lambda o: '{:.6f}{}'.format(o[1], o[0].name)
                    lst = []
                    for dn in distnames:
                        lst.append((dn, texdiff(dn.name, name)))
                        lst.append((dn, texdiff(dn.name, min_name)))
                        lst.append((dn, texdiff(get_min_name(dn.name), min_name)))
                    dn, diff = sorted(lst, key=sort)[0]
                    if diff < diffmax:
                        return dn


class DatabaseEngine:

    def __init__(self):
        self.url = None
        self.engine = None
        self.session = sessionmaker()

    def get_session(self, create_all_tables=False):
        url = os.environ["DATABASE_URL"]
        if url != self.url:
            if self.engine:
                self.engine.dispose()
            self.url = url
            self.engine = create_engine(self.url)
            if create_all_tables:
                Base.metadata.create_all(self.engine)
            self.session.configure(bind=self.engine)
        return self.session()

