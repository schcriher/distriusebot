# -*- coding: UTF-8 -*-
# Copyright (C) 2017-2018 Schmidt Cristian Hernán

usage = """Soy un bot para listar las distribuciones de GNU/Linux que usan los miembros de un grupo.

Entiendo los siguientes comandos:

• <code>/add</code> <i>distro</i> :  Agrego <i>distro</i> a tu lista de distros.
• <code>/rem</code> <i>distro</i> :  Elimino <i>distro</i> de tu lista de distros.
• <code>/fav</code> <i>distro</i> :  Defino <i>distro</i> como tu distro favorita.

• <code>/rem</code> <i>*</i> :  Elimino todo, incluida la favorita (¡cuidado!).
• <code>/fav</code> <i>-</i> :  Elimino tu distro favorita, pero no de tu lista.

• <code>/example</code> :  Muestro algunos ejemplos de uso.
• <code>/ask</code> <i>si|no</i> :  ¿Puedo hacerte acordar que establezcas tu distro?

• <code>/alternative</code> <i>nombre</i> :  Busco una alternativa a <i>nombre</i> en alternativeto.net.

ℹ Si agregas una distro a favoritos y no está en tu lista yo la agrego por ti.

En grupos:

• <code>/show</code> :  Muestro tu distro, agregaré un ❤️ a tu distro favorita y un ⚔️ si eres admin.
• <code>/show</code> <i>nombre</i> :  Igual que <code>/show</code> pero para otro usuario del grupo (prueba con su nick).

• <code>/dist</code> :  Muestro las distros que usan los miembros del grupo.
• <code>/dist</code> <i>distro</i> :  Muestro la información de la distribución <i>distro</i>.

• <code>/who</code> <i>distro</i> :  Busco la distribución <i>distro</i> y muestro quienes la usan en el grupo.
• <code>/find</code> <i>texto</i> :  Busco todo lo que se parezca a <i>texto</i> entre las distros.

ℹ Si usas <code>/show</code> en respuesta a un usuario se mostrará las distros de ese usuario.
{}
By @\N{ZERO WIDTH NO-BREAK SPACE}Schcriher.
"""

usage_admin = """
Para los administradores:

• <code>/show_greetings</code> <i>si|no</i> :  ¿Muestro los mensajes de bienvenida y despedida?
• <code>/show_admin_in_welcome</code> <i>si|no</i> :  ¿Agrego la lista de administradores a la bienvenida?
• <code>/show_admin_in_random_order</code> <i>si|no</i> :  ¿Muestro la lista de administradores en orden aleatorio?
• <code>/set_welcome</code> <i>texto</i> :  Defino el <i>texto</i> de bienvenida (vacío lo quita).
• <code>/set_goodbye</code> <i>texto</i> :  Defino el <i>texto</i> de despedida (vacío lo quita).
• <code>/set_goodbye_kicked</code> <i>texto</i> :  Defino el <i>texto</i> de despedida para usuarios expulsados (vacío lo quita).
• <code>/test_greetings</code>:  Pruebo los mensajes de bienvenida y despedida.
• <code>/reset_greetings</code>:  Restablezco los mensajes de bienvenida y despedida.

ℹ En el <i>texto</i> de bienvenida y de despedida se puede usar las siguientes variables:
• <code>{name}</code> :  Nombre del usuario.
• <code>{link}</code> :  link al usuario, usando su nombre, username o id.
• <code>{group}</code> :  Título del grupo.
"""

usage_examples = """
Agregar Debian, Fedora, Deepin, Mint y Manjaro a la lista de distros que has usado:
<code>/add Debian</code>
<code>/add Fedora</code>
<code>/add Deepin</code>
<code>/add Mint</code>
<code>/add Manjaro</code>

Establecer Debian como distro favorita:
<code>/fav Debian</code>

Ver las distros de schcriher:
<code>/show schcriher</code>

Ver la información de Kali:
<code>/dist Kali</code>

Ver que distros se usan en el grupo:
<code>/dist</code>
"""

