# -*- coding: UTF-8 -*-
# Copyright (C) 2017-2018 Schmidt Cristian Hernán

import re
import unicodedata

from itertools import islice
from functools import partial

NO_WORD_RE = re.compile(r"[^\w]")
NO_NAME_RE = re.compile(r"[^\w ]")
PERCENT_RE = re.compile(r"%+")
SPACES_RE = re.compile(r"\s+")
HTTPS_RE = re.compile(r"(^https?://|/$)", re.IGNORECASE)
EVIL_RE = re.compile(r"\b(mac[\/\s]*(os)?|[id]os|os/?x|win(xp|\d+|dows?)?)\b", re.IGNORECASE)
SUB0_RE = re.compile(r"([^a-z0-9])", re.IGNORECASE)
SUB1_RE = re.compile(r"(_+|^|$)", re.IGNORECASE)
SUB2_RE = re.compile(r"(%*linux%*|%*$)", re.IGNORECASE)
SUB2b_RE = re.compile(r"(^%*)", re.IGNORECASE)
SUB3_RE = re.compile(r"(%*(gnu|linux|\d+|osx?)%*)", re.IGNORECASE)
SUB4_RE = re.compile(r"(%*(kde|gnome|xfce|lxde|unity|mate|cinnamon|pantheon|openbox)%*)", re.IGNORECASE)
DATE_RE = re.compile(r"^\d{4}([-]\d{1,2}){,2}$")
SIZE_RE = re.compile(r"\b\d+([,.]\d+)?\s*[KMGTP]i?[B]\b", re.IGNORECASE)
YES_RE = re.compile(r"(y|s)[aei]|[tj]a|ok|oui|если|ναι", re.IGNORECASE)

DAY_PER_MONTH = {
     1: 31,
     2: 28,
     3: 31,
     4: 30,
     5: 31,
     6: 30,
     7: 31,
     8: 31,
     9: 30,
    10: 31,
    11: 30,
    12: 31,
}

HEALTH_RULES = (
    partial(SUB0_RE.sub, "_"),
    partial(SUB1_RE.sub, "%"),
    partial(SUB2_RE.sub, "%"),
    partial(SUB2_RE.sub, "%linux%", count=1),
    partial(SUB2_RE.sub, "%"),
    partial(SUB2b_RE.sub, "%linux%", count=1),
    partial(SUB2_RE.sub, "%"),
    partial(SUB3_RE.sub, "%"),
    partial(SUB4_RE.sub, "%"),
)


def spaces_normalized(text):
    return SPACES_RE.sub(' ', text).strip()


def percentage_normalized(text):
    return PERCENT_RE.sub('%', text).strip()


def escape_html(text):
    # https://core.telegram.org/bots/api#html-style
    text = text.replace('&', '&amp;')
    text = text.replace('<', '&lt;')
    text = text.replace('>', '&gt;')
    text = text.replace('"', '&quot;')
    return text


def is_leapyear(year):
    return (year % 4 == 0 and not year % 100 == 0) or year % 400 == 0


def get_month(num):
    try:
        month = int(num)
        if 1 <= month <= 12:
            return month
    except ValueError:
        pass


def get_day(year, month, num):
    try:
        day = int(num)
        add = 1 if is_leapyear(int(year)) and month == 2 else 0
        dmax = DAY_PER_MONTH[int(month)] + add
        if 1 <= day <= dmax:
            return day
    except (ValueError, KeyError):
        pass


def get_link(url, maxlen=40, preffix=" "):
    if url:
        maxlen = maxlen if maxlen > 4 else 40
        text = HTTPS_RE.sub("", url)
        if len(text) > maxlen:
            text = "{}…{}".format(text[:maxlen-2], text[-1])
        text = escape_html(text)
        preffix = escape_html(preffix)
        url = url.replace('"', '&quot;')
        return '{}<a href="{}">{}</a>'.format(preffix, url, text)
    else:
        return ""


def take(n, iterable):
    """Return first *n* items of the iterable as a list.

        >>> take(3, range(10))
        [0, 1, 2]
        >>> take(5, range(3))
        [0, 1, 2]

    Effectively a short replacement for ``next`` based iterator consumption
    when you want more than one item, but less than the whole iterator.

    github.com/erikrose/more-itertools
    """
    return list(islice(iterable, n))


def chunked(iterable, n):
    """Break *iterable* into lists of length *n*:

        >>> list(chunked([1, 2, 3, 4, 5, 6], 3))
        [[1, 2, 3], [4, 5, 6]]

    If the length of *iterable* is not evenly divisible by *n*, the last
    returned list will be shorter:

        >>> list(chunked([1, 2, 3, 4, 5, 6, 7, 8], 3))
        [[1, 2, 3], [4, 5, 6], [7, 8]]

    :func:`chunked` is useful for splitting up a computation on a large number
    of keys into batches, to be pickled and sent off to worker processes. One
    example is operations on rows in MySQL, which does not implement
    server-side cursors properly and would otherwise load the entire dataset
    into RAM on the client.

    github.com/erikrose/more-itertools
    """
    return iter(partial(take, n, iter(iterable)), [])


def csv_parser(csv):
    # Based on http://www.creativyst.com/Doc/Articles/CSV/CSV01.htm
    tokens = (
        ('FIELD_DELIMITER', ' *, *'),
        ('QUOTATION_MARKS', '"+'),
        ('LINE_BREAKS',     '(?:\r|\r\n|\n)'),
    )
    patterns = '|'.join('(?P<{}>{})'.format(*token) for token in tokens)
    get_token = re.compile(patterns).search
    quotes_enabled = False
    used_quotation = False
    row = []
    ini = 0
    token = get_token(csv)
    while token:
        typ = token.lastgroup
        pos = token.start()
        end = token.end()
        if typ == 'QUOTATION_MARKS':
            if (end - pos) % 2:  # odd-impar
                quotes_enabled = not quotes_enabled
                used_quotation = True
        else:
            if not quotes_enabled:
                if used_quotation:
                    used_quotation = False
                    ini += 1
                    pos -= 1
                row.append(csv[ini:pos])
                ini = end
                if typ == 'LINE_BREAKS':
                    yield row
                    row = []
        token = get_token(csv, pos=end)
    pos = len(csv)
    if used_quotation:
        ini += 1
        pos -= 1
    row.append(csv[ini:pos])
    yield row


def remove_diacritics(string):
    """Removes the Mark and Nonspacing characters from the string"""
    nfkd = unicodedata.normalize('NFKD', string)
    return ''.join(c for c in nfkd if unicodedata.category(c) != 'Mn')


def get_positions(string, letter, offset=0):
    """ Returns all positions of the letter in the string,
        offset is an integer that modifies all positions.
    """
    for i, c in enumerate(string):
        if c == letter:
            yield i + offset


def text_sorter(string):
    return remove_diacritics(string.lower())


def texdiff(a, b, insensitive=True, accents=False, onlyword=False):
    """ Returns the fraction of the difference between "a" and "b".

        An incorrect letter is more penalized in a short word than in a long one.

        Parameters:
            insensitive=True    Set case-insensitive
            accents=False       There is no distinction between words with and
                                without accents
            onlyword=False      It analyzes only the characters considered word
                                in the regular expressions

        Use:
            fraction = texdiff(a, b)

        where:
            0 <= fraction <= 1

            0.0 Zero difference, "a" and "b" are the same
            0.5 Half difference, example: the same letters in another order
            1.0 Full difference, no letter matches

       Design: Schmidt Cristian Hernán <schcriher@gmail.com>
    """
    if insensitive:
        a = a.lower()
        b = b.lower()

    if not accents:
        a = remove_diacritics(a)
        b = remove_diacritics(b)

    if onlyword:
        a = NO_WORD_RE.sub('', a)
        b = NO_WORD_RE.sub('', b)

    n = len(a + b)
    letters = set(a + b)

    a_offset = b.find(a) if a in b else 0
    b_offset = a.find(b) if b in a else 0

    diff_quantity = 0
    diff_position = 0

    for letter in letters:
        count_a = a.count(letter)
        count_b = b.count(letter)
        diff_quantity += abs(count_a - count_b)

        pos_a = set(get_positions(a, letter, a_offset))
        pos_b = set(get_positions(b, letter, b_offset))
        diff_position += len(pos_a.symmetric_difference(pos_b))

    return (diff_quantity + diff_position) / (2 * n)
