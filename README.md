# DistrIUse
Bot to list GNU/Linux distributions in telegram groups

Bot creado para listar las distribuciones de GNU/Linux que usan los miembros 
de grupos de telegram. Con la finalidad de saber que distros usan rápidamente.

Bot: https://t.me/distriusebot
Web: https://distriuse.pythonanywhere.com

Logo: [Basado en la imagen de dablim](https://dablim.deviantart.com/art/GNULinux-YinYang-Logo-390891444) con licencia [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)
