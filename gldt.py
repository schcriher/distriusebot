# -*- coding: UTF-8 -*-
# Copyright (C) 2017-2018 Schmidt Cristian Hernán

# GNU/Linux Distribution Timeline
# https://github.com/FabioLolix/LinuxTimeline

import os
import datetime

from tools import get_month, get_day, chunked, csv_parser
from database import User, Dist, Distname, find_distname

ROOT = os.path.dirname(os.path.abspath(__file__))
FILE = os.path.join(ROOT, "gldt.csv")

FIX_NAME = {
    "fedora core": "Fedora",
    "puppy": "Puppy Linux",
    "point": "Point Linux",
}

# TODO: add https://distrowatch.com/

EXTRA_DIST = (
    #"#","Name","Color","Parent","Start","Stop","Icon","Description",[Namechange","When","Description"]{3,}
    '"N","Huayra",,"Debian","2012.8.1",,,"https://es.wikipedia.org/wiki/Huayra_GNU/Linux",,,,,,,,,',
    '"N","FreeBSD",,,"1993.11",,,"https://www.freebsd.org",,,,,,,,,',
    '"N","PicarOS",,"GALPon MiniNo","2008.4",,,"https://minino.galpon.org",,,,,,,,,',
    '"N","BackSlash",,"Ubuntu","2016.11.2",,,"https://backslashlinux.com",,,,,,,,,',
    '"N","KXStudio",,"Ubuntu","2017.6.9",,,"http://kxstudio.linuxaudio.org",,,,,,,,,',
    '"N","NetHunter",,"Kali","2014",,,"https://www.kali.org/kali-linux-nethunter/",,,,,,,,,',
    '"N","SolydX",,"Debian","2013.2.14",,,"https://solydxk.com/",,,,,,,,,',
    '"N","SolydK",,"Debian","2013.2.14",,,"https://solydxk.com/",,,,,,,,,',
    '"N","SalentOS",,"Debian","2014.01.30",,,"https://www.salentos.it/",,,,,,,,,',
    '"N","NelumOS",,"Debian","2015.12.29",,,"https://sourceforge.net/projects/nelum-os/",,,,,,,,,',
    '"N","Haiku",,,"2002.4.29",,,"https://www.haiku-os.org/",,,,,,,,,',
    '"N","EndeavourOS",,Antergos,"2019.7.15",,,"https://endeavouros.com/",,,,,,,,,',
    '"N","Feren OS",,Mint,"2015.2",,,"http://ferenos.weebly.com/",,,,,,,,,',
)


def get_date(text, line, errors, current_year):
    year = None
    month = None
    day = None
    if text:
        error = []
        if '-' in text:
            error.append("dash to point")
            text = text.replace('-', '.')

        nums = text.split(".")
        n = len(nums)

        if n > 0:
            year = int(nums[0])
            if not (1950 <= year <= current_year):
                year = None
                error.append("in year")

        if n > 1 and year is not None:
            month = get_month(nums[1])
            if not month:
                error.append("in month")

        if n > 2 and month is not None:
            day = get_day(year, month, nums[2])
            if not day:
                error.append("in day")

        if error:
            errors.append("line {}: {}".format(line, ', '.join(error)))

    return year, month, day


def get_name(name):
    return FIX_NAME.get(name.lower(), name)


def get_distname(db, name):
    return db.query(Distname).filter(Distname.name.ilike(name)).first()


def extract(db):
    backup = {}
    for user in db.query(User).all():
        fav = user.favourite.name if user.favourite else None
        backup[user.id] = (fav, tuple(dn.name for dn in user.distnames))
        user.distnames.clear()
        user.favourite = None
    return backup


def restore(db, backup, errors):
    err = "UID {}: {} not found for {}"
    for user in db.query(User).all():
        fav, distnames = backup.get(user.id, [None, []])
        for distname in distnames:
            dn = find_distname(db, distname, diffmax=0.16)
            if dn:
                user.distnames.append(dn)
            else:
                errors.append(err.format(user.id, distname, 'distnames'))
        if fav:
            dn = find_distname(db, fav, diffmax=0.16)
            if dn:
                user.favourite = dn
            else:
                errors.append(err.format(user.id, fav, 'favourite'))


def add_dist(db, row, line, errors):
    current_year = datetime.datetime.now().year

    _, name, _, parent, start, stop, _, description, *namechanges = row
    names = [name, start, description, *namechanges]

    stop_date = get_date(stop, line, errors, current_year)
    for i in range(0, len(names), 3):
        if name:
            name = get_name(names[i])
            names[i] = name
            distname = get_distname(db, name)
            if distname:
                dist = distname.dist
                break
    else:
        dist = Dist()
        db.add(dist)
    dist.stop_year = stop_date[0]
    dist.stop_month = stop_date[1]
    dist.stop_day = stop_date[2]

    parent_name = get_distname(db, get_name(parent))
    if parent_name:
        dist.parent_id = parent_name.dist.id

    for name, start, description in chunked(names, 3):
        if name:
            distname = get_distname(db, name)
            if not distname:
                distname = Distname()
                dist.names.append(distname)
                db.add(distname)
            start_date = get_date(start, line, errors, current_year)
            distname.name = name
            distname.start_year = start_date[0]
            distname.start_month = start_date[1]
            distname.start_day = start_date[2]
            distname.url = description


def set_gldt(db, include):
    backup = extract(db)
    db.query(Distname).delete()
    db.query(Dist).delete()
    with open(FILE, encoding='utf-8', mode='r') as fid:
        gldt = fid.read().strip()
    data = "{}\n{}".format(gldt, "\n".join(EXTRA_DIST)).strip()
    errors = []
    for line, row in enumerate(csv_parser(data), 1):
        if len(row) > 7 and row[0] in include:
            add_dist(db, row, line, errors)
    restore(db, backup, errors)
    return errors
