# -*- coding: UTF-8 -*-
# Copyright (C) 2017-2018 Schmidt Cristian Hernán

from datetime import datetime, timedelta
from functools import wraps, lru_cache


def timed_cache(maxsize, minutes):
    # https://gist.github.com/Morreski/c1d08a3afa4040815eafd3891e16b945

    def _wrapper(f):
        update_delta = timedelta(minutes=minutes)
        next_update = datetime.utcnow() + update_delta

        f = lru_cache(maxsize=maxsize)(f)

        @wraps(f)
        def _wrapped(*args, **kwargs):
            nonlocal next_update
            now = datetime.utcnow()
            if now > next_update:
                f.cache_clear()
                next_update = now + update_delta
            return f(*args, **kwargs)
        return _wrapped
    return _wrapper


class TelegramCache:

    def __init__(self):
        self.bot = None

    @timed_cache(maxsize=None, minutes=10)
    def is_admin(self, chat_id, user_id):
        chatmember = self.bot.get_chat_member(chat_id=chat_id, user_id=user_id)
        if chatmember.status in (chatmember.ADMINISTRATOR, chatmember.CREATOR):
            return True
        else:
            return False

    @timed_cache(maxsize=None, minutes=10)
    def get_admins(self, chat_id):
        chatmembers = self.bot.getChatAdministrators(chat_id)
        return tuple(chatmember.user for chatmember in chatmembers)
